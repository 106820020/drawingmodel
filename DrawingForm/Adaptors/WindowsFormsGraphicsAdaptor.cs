﻿using System.Windows.Forms;
using System.Drawing;
using DrawingModel;
using System;
using System.Collections.Generic;

namespace DrawingForm.Adaptors
{
    class WindowsFormsGraphicsAdaptor : IGraphics
    {
        Graphics _graphics;
        const int TWO = 2;
        const int THREE = 3;
        const int FIVE = 5;
        const double TWO_FIVE = 2.5;
        public WindowsFormsGraphicsAdaptor(Graphics graphics)
        {
            this._graphics = graphics;
        }

        // clear all
        public void ClearAll()
        {
            // OnPaint時會自動清除畫面，因此不需實作
        }

        // draw line
        public void DrawLine(double x1, double y1, double x2, double y2)
        {
            _graphics.DrawLine(Pens.DarkGreen, (float)x1, (float)y1, (float)x2, (float)y2);
        }

        //draw the frame
        public void DrawLineFrame(double x1, double y1, double x2, double y2)
        {
            Pen dashPen = new Pen(Color.Red, (float)TWO_FIVE);
            dashPen.DashPattern = new float[] { (float)THREE, (float)THREE };
            _graphics.DrawRectangle(dashPen, (float)Math.Min(x1, x2), (float)Math.Min(y1, y2), (float)Math.Abs(x2 - x1), (float)Math.Abs(y2 - y1));
            _graphics.FillEllipse(Brushes.White, (float)(x1 - THREE), (float)(y1 - THREE), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x1 - THREE), (float)(y2 - THREE), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x2 - THREE), (float)(y1 - THREE), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x2 - THREE), (float)(y2 - THREE), FIVE, FIVE);
            _graphics.DrawEllipse(Pens.Black, (float)(x1 - THREE), (float)(y1 - THREE), FIVE, FIVE);
            _graphics.DrawEllipse(Pens.Black, (float)(x1 - THREE), (float)(y2 - THREE), FIVE, FIVE);
            _graphics.DrawEllipse(Pens.Black, (float)(x2 - THREE), (float)(y1 - THREE), FIVE, FIVE);
            _graphics.DrawEllipse(Pens.Black, (float)(x2 - THREE), (float)(y2 - THREE), FIVE, FIVE);
        }

        // draw rec
        public void DrawRectangle(double x1, double y1, double x2, double y2)
        {
            if (x2 > x1 && y2 > y1)
                _graphics.FillRectangle(Brushes.Black, (float)x1, (float)y1, (float)(x2 - x1), (float)(y2 - y1));
            else if (x1 > x2 && y2 > y1)
                _graphics.FillRectangle(Brushes.Black, (float)x2, (float)y1, (float)(x1 - x2), (float)(y2 - y1));
            else if (x2 > x1 && y1 > y2)
                _graphics.FillRectangle(Brushes.Black, (float)x1, (float)y2, (float)(x2 - x1), (float)(y1 - y2));
            else if (x1 > x2 && y1 > y2)
                _graphics.FillRectangle(Brushes.Black, (float)x2, (float)y2, (float)(x1 - x2), (float)(y1 - y2));
        }

        //draw the frame
        public void DrawRectangleFrame(double x1, double y1, double x2, double y2)
        {
            Pen dashPen = new Pen(Color.Red, (float)TWO_FIVE);
            dashPen.DashPattern = new float[] { (float)THREE, (float)THREE };
            if (x2 > x1 && y2 > y1)
                _graphics.DrawRectangle(dashPen, (float)x1, (float)y1, (float)(x2 - x1), (float)(y2 - y1));
            else if (x1 > x2 && y2 > y1)
                _graphics.DrawRectangle(dashPen, (float)x2, (float)y1, (float)(x1 - x2), (float)(y2 - y1));
            else if (x2 > x1 && y1 > y2)
                _graphics.DrawRectangle(dashPen, (float)x1, (float)y2, (float)(x2 - x1), (float)(y1 - y2));
            else if (x1 > x2 && y1 > y2)
                _graphics.DrawRectangle(dashPen, (float)x2, (float)y2, (float)(x1 - x2), (float)(y1 - y2));
            FillRectangleFrame(x1, y1, x2, y2);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x1 - THREE), (float)(y1 - THREE), FIVE, FIVE);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x2 - TWO), (float)(y2 - TWO), FIVE, FIVE);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x2 - TWO), (float)(y1 - TWO), FIVE, FIVE);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x1 - TWO), (float)(y2 - TWO), FIVE, FIVE);
        }

        //fill circle
        private void FillRectangleFrame(double x1, double y1, double x2, double y2)
        {
            _graphics.FillEllipse(Brushes.White, (float)(x1 - THREE), (float)(y1 - THREE), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x2 - TWO), (float)(y2 - TWO), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x2 - TWO), (float)(y1 - TWO), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x1 - TWO), (float)(y2 - TWO), FIVE, FIVE);
        }

        // draw hexagon
        public void DrawHexagon(double x1, double y1, double x2, double y2)
        {
            PointF[] points = new PointF[]{ new PointF((float)x1, (float)y1), new PointF((float)(x1 + x2) / TWO, (float)((y1 - y2) / TWO + y1)), new PointF((float)x2, (float)y1), new PointF((float)x2, (float)y2), new PointF((float)(x1 + x2) / TWO, (float)(y2 - (y1 - y2) / TWO)), new PointF((float)x1, (float)y2) };
            _graphics.DrawPolygon(new Pen(Color.Black), points);
            _graphics.FillPolygon(Brushes.Blue, points);
        }

        //draw the frame
        public void DrawHexagonFrame(double x1, double y1, double x2, double y2)
        {
            Pen dashPen = new Pen(Color.Red, (float)TWO_FIVE);
            dashPen.DashPattern = new float[] { (float)THREE, (float)THREE };
            PointF[] points = new PointF[]{ new PointF((float)x1,(float)((y1 - y2) / TWO + y1)), new PointF((float)x2,(float)((y1 - y2) / TWO + y1)), new PointF((float)x2,(float)(y2 - (y1 - y2) / TWO)), new PointF((float)x1,(float)(y2 - (y1 - y2) / TWO)) };
            _graphics.DrawPolygon(dashPen, points);
            FillHexgaonFrame(x1, y1, x2, y2);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x1 - TWO), (float)(((y1 - y2) / TWO + y1) - TWO), FIVE, FIVE);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x2 - TWO), (float)(((y1 - y2) / TWO + y1) - TWO), FIVE, FIVE);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x2 - TWO), (float)((y2 - (y1 - y2) / TWO) - TWO), FIVE, FIVE);
            _graphics.DrawEllipse(new Pen(Color.Black), (float)(x1 - TWO), (float)((y2 - (y1 - y2) / TWO) - TWO), FIVE, FIVE);
        }

        // fill circle
        private void FillHexgaonFrame(double x1, double y1, double x2, double y2)
        {
            _graphics.FillEllipse(Brushes.White, (float)(x1 - TWO), (float)(((y1 - y2) / TWO + y1) - TWO), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x2 - TWO), (float)(((y1 - y2) / TWO + y1) - TWO), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x2 - TWO), (float)((y2 - (y1 - y2) / TWO) - TWO), FIVE, FIVE);
            _graphics.FillEllipse(Brushes.White, (float)(x1 - TWO), (float)((y2 - (y1 - y2) / TWO) - TWO), FIVE, FIVE);
        }

        public void DrawCompound(List<IShape> shapes)
        {
            foreach (IShape shape in shapes)
            {
                if (shape.GetType() == typeof(DrawingModel.Rectangle))
                    DrawRectangle(shape.X1, shape.Y1, shape.X2, shape.Y2);
                else if (shape.GetType() == typeof(DrawingModel.Line))
                    DrawLine(shape.X1, shape.Y1, shape.X2, shape.Y2);
                else if (shape.GetType() == typeof(DrawingModel.Hexagon))
                    DrawHexagon(shape.X1, shape.Y1, shape.X2, shape.Y2);
                else if (shape.GetType() == typeof(DrawingModel.Compound))
                    DrawCompound(shape.GetChild());
            }
        }
    }
}