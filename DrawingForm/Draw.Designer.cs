﻿namespace DrawingForm
{
    partial class Draw
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Draw));
            this._tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this._combineButton = new System.Windows.Forms.Button();
            this._rectangleButton = new System.Windows.Forms.Button();
            this._lineButton = new System.Windows.Forms.Button();
            this._clearButton = new System.Windows.Forms.Button();
            this._hexagonButton = new System.Windows.Forms.Button();
            this._tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this._toolStrip = new System.Windows.Forms.ToolStrip();
            this._undo = new System.Windows.Forms.ToolStripButton();
            this._redo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.import = new System.Windows.Forms.ToolStripButton();
            this.export = new System.Windows.Forms.ToolStripDropDownButton();
            this.ToImage = new System.Windows.Forms.ToolStripMenuItem();
            this.ToJson = new System.Windows.Forms.ToolStripMenuItem();
            this._mutiSelectButton = new System.Windows.Forms.ToolStripButton();
            this._deleteButton = new System.Windows.Forms.ToolStripButton();
            this._selectInfo = new System.Windows.Forms.Label();
            this._tableLayoutPanel1.SuspendLayout();
            this._tableLayoutPanel2.SuspendLayout();
            this._tableLayoutPanel3.SuspendLayout();
            this._toolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tableLayoutPanel1
            // 
            this._tableLayoutPanel1.ColumnCount = 1;
            this._tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel2, 0, 1);
            this._tableLayoutPanel1.Controls.Add(this._tableLayoutPanel3, 0, 0);
            this._tableLayoutPanel1.Controls.Add(this._selectInfo, 0, 3);
            this._tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this._tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel1.Name = "_tableLayoutPanel1";
            this._tableLayoutPanel1.RowCount = 4;
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.77778F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.22222F));
            this._tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this._tableLayoutPanel1.Size = new System.Drawing.Size(533, 300);
            this._tableLayoutPanel1.TabIndex = 0;
            // 
            // _tableLayoutPanel2
            // 
            this._tableLayoutPanel2.ColumnCount = 5;
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this._tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this._tableLayoutPanel2.Controls.Add(this._combineButton, 3, 0);
            this._tableLayoutPanel2.Controls.Add(this._rectangleButton, 0, 0);
            this._tableLayoutPanel2.Controls.Add(this._lineButton, 2, 0);
            this._tableLayoutPanel2.Controls.Add(this._clearButton, 4, 0);
            this._tableLayoutPanel2.Controls.Add(this._hexagonButton, 1, 0);
            this._tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel2.Location = new System.Drawing.Point(2, 26);
            this._tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel2.Name = "_tableLayoutPanel2";
            this._tableLayoutPanel2.RowCount = 1;
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this._tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this._tableLayoutPanel2.Size = new System.Drawing.Size(529, 29);
            this._tableLayoutPanel2.TabIndex = 0;
            // 
            // _combineButton
            // 
            this._combineButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._combineButton.Location = new System.Drawing.Point(317, 2);
            this._combineButton.Margin = new System.Windows.Forms.Padding(2);
            this._combineButton.Name = "_combineButton";
            this._combineButton.Size = new System.Drawing.Size(101, 25);
            this._combineButton.TabIndex = 3;
            this._combineButton.Text = "Combine";
            this._combineButton.UseVisualStyleBackColor = true;
            this._combineButton.Click += new System.EventHandler(this.ClickCombineButton);
            // 
            // _rectangleButton
            // 
            this._rectangleButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rectangleButton.Location = new System.Drawing.Point(2, 2);
            this._rectangleButton.Margin = new System.Windows.Forms.Padding(2);
            this._rectangleButton.Name = "_rectangleButton";
            this._rectangleButton.Size = new System.Drawing.Size(101, 25);
            this._rectangleButton.TabIndex = 0;
            this._rectangleButton.Text = "Rectangle";
            this._rectangleButton.UseVisualStyleBackColor = true;
            this._rectangleButton.Click += new System.EventHandler(this.ClickRectangleButton);
            // 
            // _lineButton
            // 
            this._lineButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lineButton.Location = new System.Drawing.Point(212, 2);
            this._lineButton.Margin = new System.Windows.Forms.Padding(2);
            this._lineButton.Name = "_lineButton";
            this._lineButton.Size = new System.Drawing.Size(101, 25);
            this._lineButton.TabIndex = 1;
            this._lineButton.Text = "Line";
            this._lineButton.UseVisualStyleBackColor = true;
            this._lineButton.Click += new System.EventHandler(this.ClickLineButton);
            // 
            // _clearButton
            // 
            this._clearButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._clearButton.Location = new System.Drawing.Point(422, 2);
            this._clearButton.Margin = new System.Windows.Forms.Padding(2);
            this._clearButton.Name = "_clearButton";
            this._clearButton.Size = new System.Drawing.Size(105, 25);
            this._clearButton.TabIndex = 0;
            this._clearButton.Text = "Clear";
            this._clearButton.UseVisualStyleBackColor = true;
            this._clearButton.Click += new System.EventHandler(this.ClickClearButton);
            // 
            // _hexagonButton
            // 
            this._hexagonButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this._hexagonButton.Location = new System.Drawing.Point(107, 2);
            this._hexagonButton.Margin = new System.Windows.Forms.Padding(2);
            this._hexagonButton.Name = "_hexagonButton";
            this._hexagonButton.Size = new System.Drawing.Size(101, 25);
            this._hexagonButton.TabIndex = 2;
            this._hexagonButton.Text = "Hexagon";
            this._hexagonButton.UseVisualStyleBackColor = true;
            this._hexagonButton.Click += new System.EventHandler(this.ClickHexagonButton);
            // 
            // _tableLayoutPanel3
            // 
            this._tableLayoutPanel3.ColumnCount = 1;
            this._tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel3.Controls.Add(this._toolStrip, 0, 0);
            this._tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this._tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this._tableLayoutPanel3.Name = "_tableLayoutPanel3";
            this._tableLayoutPanel3.RowCount = 1;
            this._tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tableLayoutPanel3.Size = new System.Drawing.Size(529, 20);
            this._tableLayoutPanel3.TabIndex = 1;
            // 
            // _toolStrip
            // 
            this._toolStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this._toolStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this._toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._undo,
            this._redo,
            this.toolStripSeparator1,
            this.import,
            this.export,
            this._mutiSelectButton,
            this._deleteButton});
            this._toolStrip.Location = new System.Drawing.Point(0, 0);
            this._toolStrip.Name = "_toolStrip";
            this._toolStrip.Size = new System.Drawing.Size(529, 20);
            this._toolStrip.TabIndex = 0;
            // 
            // _undo
            // 
            this._undo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._undo.Enabled = false;
            this._undo.Image = ((System.Drawing.Image)(resources.GetObject("_undo.Image")));
            this._undo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._undo.Name = "_undo";
            this._undo.Size = new System.Drawing.Size(28, 17);
            this._undo.Text = "toolStripButton1";
            this._undo.Click += new System.EventHandler(this.UndoHandler);
            // 
            // _redo
            // 
            this._redo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._redo.Enabled = false;
            this._redo.Image = ((System.Drawing.Image)(resources.GetObject("_redo.Image")));
            this._redo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._redo.Name = "_redo";
            this._redo.Size = new System.Drawing.Size(28, 17);
            this._redo.Text = "toolStripButton2";
            this._redo.Click += new System.EventHandler(this.RedoHandler);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 20);
            // 
            // import
            // 
            this.import.Image = global::DrawingForm.Properties.Resources.import;
            this.import.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.import.Name = "import";
            this.import.Size = new System.Drawing.Size(73, 17);
            this.import.Text = "Import";
            this.import.Click += new System.EventHandler(this.ImportJson);
            // 
            // export
            // 
            this.export.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToImage,
            this.ToJson});
            this.export.Image = global::DrawingForm.Properties.Resources.export;
            this.export.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.export.Name = "export";
            this.export.Size = new System.Drawing.Size(81, 17);
            this.export.Text = "Export";
            // 
            // ToImage
            // 
            this.ToImage.Name = "ToImage";
            this.ToImage.Size = new System.Drawing.Size(211, 22);
            this.ToImage.Text = "Image (.jpg, .png, .bmp)";
            this.ToImage.Click += new System.EventHandler(this.ExportAsImage);
            // 
            // ToJson
            // 
            this.ToJson.Name = "ToJson";
            this.ToJson.Size = new System.Drawing.Size(211, 22);
            this.ToJson.Text = "JSON (.json)";
            this.ToJson.Click += new System.EventHandler(this.ExportAsJson);
            // 
            // _mutiSelectButton
            // 
            this._mutiSelectButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this._mutiSelectButton.Image = ((System.Drawing.Image)(resources.GetObject("_mutiSelectButton.Image")));
            this._mutiSelectButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._mutiSelectButton.Name = "_mutiSelectButton";
            this._mutiSelectButton.Size = new System.Drawing.Size(71, 17);
            this._mutiSelectButton.Text = "MutiSelect";
            this._mutiSelectButton.Click += new System.EventHandler(this.ClickMutiSelectButton);
            // 
            // _deleteButton
            // 
            this._deleteButton.Image = ((System.Drawing.Image)(resources.GetObject("_deleteButton.Image")));
            this._deleteButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._deleteButton.Name = "_deleteButton";
            this._deleteButton.Size = new System.Drawing.Size(72, 17);
            this._deleteButton.Text = "Delete";
            this._deleteButton.Click += new System.EventHandler(this.ClickDeleteButton);
            // 
            // _selectInfo
            // 
            this._selectInfo.AccessibleName = "_selectInfo";
            this._selectInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._selectInfo.AutoSize = true;
            this._selectInfo.Location = new System.Drawing.Point(531, 267);
            this._selectInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this._selectInfo.Name = "_selectInfo";
            this._selectInfo.Size = new System.Drawing.Size(0, 12);
            this._selectInfo.TabIndex = 2;
            // 
            // Draw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 300);
            this.Controls.Add(this._tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Draw";
            this.Text = "DrawingForm";
            this._tableLayoutPanel1.ResumeLayout(false);
            this._tableLayoutPanel1.PerformLayout();
            this._tableLayoutPanel2.ResumeLayout(false);
            this._tableLayoutPanel3.ResumeLayout(false);
            this._tableLayoutPanel3.PerformLayout();
            this._toolStrip.ResumeLayout(false);
            this._toolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel2;
        private System.Windows.Forms.Button _rectangleButton;
        private System.Windows.Forms.Button _lineButton;
        private System.Windows.Forms.Button _clearButton;
        private System.Windows.Forms.Button _hexagonButton;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel3;
        private System.Windows.Forms.ToolStrip _toolStrip;
        private System.Windows.Forms.ToolStripButton _undo;
        private System.Windows.Forms.ToolStripButton _redo;
        private System.Windows.Forms.Label _selectInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton _mutiSelectButton;
        private System.Windows.Forms.ToolStripDropDownButton export;
        private System.Windows.Forms.ToolStripMenuItem ToImage;
        private System.Windows.Forms.ToolStripMenuItem ToJson;
        private System.Windows.Forms.ToolStripButton import;
        private System.Windows.Forms.ToolStripButton _deleteButton;
        private System.Windows.Forms.Button _combineButton;
    }
}

