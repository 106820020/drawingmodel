﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DrawingModel;
using DrawingForm.PresentationModel;
using DrawingForm.Adaptors;
using System.IO;
using System.Drawing.Imaging;
using DrawingModel.States;

namespace DrawingForm
{
    public partial class Draw : Form
    {
        DoubleBufferedPanel _canvas;
        PresentationModel.PresentationModel _presentationModel;
        const int TWO_FIVE_FIVE = 255;
        const int TWO_TWO_FOUR = 224;
        const int ONE_NINE_TWO = 192;
        const int TWO = 2;
        const int THREE = 3;
        const int SIX_FIVE = 65;
        const int SEVEN_NINE_FOUR = 794;
        const int THREE_EIGHT_TWO = 382;
        const string CANVAS = "_canvas";
        public Draw()
        {
            InitializeComponent();
            _canvas = new DoubleBufferedPanel();

            this._canvas.AccessibleName = CANVAS;
            this._canvas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(TWO_FIVE_FIVE)))), ((int)(((byte)(TWO_TWO_FOUR)))), ((int)(((byte)(ONE_NINE_TWO)))));
            this._canvas.Dock = System.Windows.Forms.DockStyle.Fill;
            this._canvas.Location = new System.Drawing.Point(THREE, SIX_FIVE);
            this._canvas.Size = new System.Drawing.Size(SEVEN_NINE_FOUR, THREE_EIGHT_TWO);
            this._canvas.TabIndex = 1;
            this._canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.HandleCanvasPaint);
            this._canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.HandleCanvasPressed);
            this._canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.HandleCanvasMoved);
            this._canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HandleCanvasReleased);
            this._tableLayoutPanel1.Controls.Add(_canvas, 0, TWO);

            //
            // prepare presentation model and model
            //
            _presentationModel = new PresentationModel.PresentationModel();
            Model.Instance()._modelChanged += HandleModelChanged;
            UpdateButtons();
        }

        // click clear button
        public void ClickClearButton(object sender, System.EventArgs e)
        {
            Model.Instance().SetState(Model.Instance().GetClearState());
            Model.Instance().Clear();
            _presentationModel.ClickClear();
            UpdateButtons();
            _selectInfo.Text = _presentationModel.GetSelectInfo();
        }

        // handle canvas pressed
        public void HandleCanvasPressed(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Model.Instance().PressPointer(e.X, e.Y);
        }

        // handle canvas released
        public void HandleCanvasReleased(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Model.Instance().ReleasePointer(e.X, e.Y);
            _presentationModel.SetCursor();

            
            if (Model.Instance().GetState().GetType() == typeof(MutiSelectState))
            {
                if (!Model.Instance().ClickShapes(e.X, e.Y))
                    Model.Instance().SetState(Model.Instance().GetCursorState());
            }
            else if (Model.Instance().GetState().GetType() != typeof(CursorState))
            {
                Model.Instance().SetState(Model.Instance().GetCursorState());
            }

            _selectInfo.Text = _presentationModel.GetSelectInfo();
            UpdateButtons();
        }

        // handle canvas moved
        public void HandleCanvasMoved(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            Model.Instance().MovePointer(e.X, e.Y);
        }

        // handle canvas paint
        public void HandleCanvasPaint(object sender, PaintEventArgs e)
        {
            _presentationModel.Draw(new WindowsFormsGraphicsAdaptor(e.Graphics));
        }

        // handle model changed
        public void HandleModelChanged()
        {
            Invalidate(true);
            UpdateButtons();
        }

        // click rectangle button
        private void ClickRectangleButton(object sender, EventArgs e)
        {
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().mode = Mode.Rectangle;
            _presentationModel.ClickRectangle();
            UpdateButtons();
        }

        // click line button
        private void ClickLineButton(object sender, EventArgs e)
        {
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().mode = Mode.Line;
            _presentationModel.ClickLine();
            UpdateButtons();
        }

        // click line button
        private void ClickHexagonButton(object sender, EventArgs e)
        {
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().mode = Mode.Hexagon;
            _presentationModel.ClickHexagon();
            UpdateButtons();
        }

        // update buttons
        private void UpdateButtons()
        {
            _lineButton.Enabled = _presentationModel.IsLine();
            _rectangleButton.Enabled = _presentationModel.IsRectangle();
            _hexagonButton.Enabled = _presentationModel.IsHexagon();
            _undo.Enabled = Model.Instance().IsUndo;
            _redo.Enabled = Model.Instance().IsRedo;
            _selectInfo.Text = _presentationModel.GetSelectInfo();
        }

        // undo
        private void UndoHandler(object sender, EventArgs e)
        {
            Model.Instance().Undo();
            UpdateButtons();
        }

        // redo
        private void RedoHandler(object sender, EventArgs e)
        {
            Model.Instance().Redo();
            UpdateButtons();
        }

        private void ClickMutiSelectButton(object sender, EventArgs e)
        {
            Model.Instance().SetState(Model.Instance().GetMutiSelectState());
            _presentationModel.ClickMutiSelect();
            UpdateButtons();
            _selectInfo.Text = _presentationModel.GetSelectInfo();
        }

        //Export as image
        private void ExportAsImage(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.Filter = "PNG|*.png|Bitmap|*.bmp|JPEG|*.jpg;*.jpeg";
            DialogResult dr = diag.ShowDialog();

            if (dr.Equals(DialogResult.OK))
            {
                string filename = diag.FileName;

                // filename not specified. Use FileName = ...
                if (filename == null || filename.Length == 0)
                    throw new Exception("Unspecified file name");

                // cannot override RO file
                if (File.Exists(filename)
                    && (File.GetAttributes(filename)
                    & FileAttributes.ReadOnly) != 0)
                    throw new Exception("File exists and is read-only!");

                Bitmap bmp = new Bitmap(_canvas.Width, _canvas.Height);//to create bmp of same size as panel
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, _canvas.Width, _canvas.Height); //to set bounds to image
                _canvas.DrawToBitmap(bmp, rect);      // drawing panel image into bmp of bounds of rect

                switch (diag.FilterIndex)
                {
                    case 1:
                        bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png); //save as png
                        break;

                    case 2:
                        bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Bmp); //save as bitmap
                        break;

                    case 3:
                        bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Jpeg); //save as jpeg
                        break;
                }
            }
        }

        private void ExportAsJson(object sender, EventArgs e)
        {
            SaveFileDialog diag = new SaveFileDialog();
            diag.Filter = "JSON | *.json";
            DialogResult dr = diag.ShowDialog();
            if (dr.Equals(DialogResult.OK))
            {
                string filename = diag.FileName;
                // filename not specified. Use FileName = ...
                if (filename == null || filename.Length == 0)
                    throw new Exception("Unspecified file name");

                // cannot override RO file
                if (File.Exists(filename)
                    && (File.GetAttributes(filename)
                    & FileAttributes.ReadOnly) != 0)
                    throw new Exception("File exists and is read-only!");

                Model.Instance().ExportFile(filename);
            }
        }

        private void ImportJson(object sender, EventArgs e)
        {
            OpenFileDialog diag = new OpenFileDialog();
            diag.Filter = "JSON | *.json";
            DialogResult dr = diag.ShowDialog();
            if (dr.Equals(DialogResult.OK))
            {
                string filename = diag.FileName;
                // filename not specified. Use FileName = ...
                if (filename == null || filename.Length == 0)
                    throw new Exception("Unspecified file name");

                Model.Instance().ImportFile(filename);
            }
        }

        private void ClickCombineButton(object sender, EventArgs e)
        {
            Model.Instance().ClickCombine();
            Model.Instance().SetState(Model.Instance().GetCursorState());
            _presentationModel.ClickCombine();
            UpdateButtons();
            _selectInfo.Text = _presentationModel.GetSelectInfo();
        }

        //click delete button
        private void ClickDeleteButton(object sender, EventArgs e)
        {
            Model.Instance().Delete();
            UpdateButtons();
        }
    }
}
