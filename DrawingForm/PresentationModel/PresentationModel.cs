﻿using DrawingForm.Adaptors;
using DrawingModel;
using System.Windows.Forms;

namespace DrawingForm.PresentationModel
{
    public class PresentationModel
    {
        string _selectInfo = string.Empty;
        bool _line = true;
        bool _rectangle = true;
        bool _hexagon = true;
        const string SELECT = "Select:";
        const string COMMA = ",";
        const string LEFT = " (";
        const string RIGHT = ")";
        public PresentationModel()
        {
        }

        //draw
        public void Draw(IGraphics graphics)
        {
            Model.Instance().Draw(graphics);
        }

        //click clear
        public void ClickClear()
        {
            _line = true;
            _rectangle = true;
            _hexagon = true;
        }

        // click line
        public void ClickLine()
        {
            _line = false;
            _rectangle = true;
            _hexagon = true;
            Model.Instance().ClickButton();
        }

        // click rec
        public void ClickRectangle()
        {
            _line = true;
            _rectangle = false;
            _hexagon = true;
            Model.Instance().ClickButton();
        }

        // click hexagon
        public void ClickHexagon()
        {
            _line = true;
            _rectangle = true;
            _hexagon = false;
            Model.Instance().ClickButton();
        }

        public void ClickMutiSelect()
        {
            _line = true;
            _rectangle = true;
            _hexagon = true;
            Model.Instance().ClickButton();
        }

        //click combine
        public void ClickCombine()
        {
            _line = true;
            _rectangle = true;
            _hexagon = true;
        }

        // set cursor
        public void SetCursor()
        {
            _line = true;
            _rectangle = true;
            _hexagon = true;
        }

        //set select info
        public string GetSelectInfo()
        {
            IShape select = Model.Instance().GetSelectShape();
            if (select != null)
                _selectInfo = SELECT + select.GetType().Name + LEFT + select.X1 + COMMA + select.Y1 + COMMA + select.X2 + COMMA + select.Y2 + RIGHT;
            else
                _selectInfo = SELECT;
            return _selectInfo;
        }

        // if it is line
        public bool IsLine()
        {
            return _line;
        }

        // if it is hexagon
        public bool IsHexagon()
        {
            return _hexagon;
        }

        // if it is rec
        public bool IsRectangle()
        {
            return _rectangle;
        }
    }
}