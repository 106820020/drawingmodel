﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingForm.PresentationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawingModel;
using DrawingFormTests.PresentationModel;

namespace DrawingForm.PresentationModel.Tests
{
    [TestClass()]
    public class PresentationModelTests
    {
        PresentationModel _presentationModel;
        FakeGraphicsAdaptor _iGraphics;

        [TestInitialize()]
        public void Initialize()
        {
            _presentationModel = new PresentationModel();
            _iGraphics = new FakeGraphicsAdaptor();
        }

        [TestMethod()]
        public void PresentationModelTest()
        {
            Assert.AreNotEqual(_presentationModel, null);
        }

        [TestMethod()]
        public void DrawTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().MovePointer(50, 50);
            _presentationModel.Draw(_iGraphics);
            Assert.AreEqual(25, _iGraphics.X1);
            Assert.AreEqual(25, _iGraphics.Y1);
            Assert.AreEqual(50, _iGraphics.X2);
            Assert.AreEqual(50, _iGraphics.Y2);
            Model.Instance().ReleasePointer(40, 40);
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            _presentationModel.Draw(_iGraphics);
            Assert.AreEqual(20, _iGraphics.X1);
            Assert.AreEqual(20, _iGraphics.Y1);
            Assert.AreEqual(10, _iGraphics.X2);
            Assert.AreEqual(10, _iGraphics.Y2);
            Model.Instance().ReleasePointer(15, 15);
        }

        [TestMethod()]
        public void ClickClearTest()
        {
            _presentationModel.ClickClear();
            Assert.AreEqual(_presentationModel.IsLine(), true);
            Assert.AreEqual(_presentationModel.IsRectangle(), true);
        }

        [TestMethod()]
        public void ClickLineTest()
        {
            _presentationModel.ClickLine();
            Assert.AreEqual(_presentationModel.IsLine(), false);
            Assert.AreEqual(_presentationModel.IsRectangle(), true);
        }

        [TestMethod()]
        public void ClickRectangleTest()
        {
            _presentationModel.ClickRectangle();
            Assert.AreEqual(_presentationModel.IsLine(), true);
            Assert.AreEqual(_presentationModel.IsRectangle(), false);
        }
        public void ClickHexagonTest()
        {
            _presentationModel.ClickHexagon();
            Assert.AreEqual(_presentationModel.IsLine(), true);
            Assert.AreEqual(_presentationModel.IsHexagon(), false);
        }

        [TestMethod()]
        public void IsLineTest()
        {
            _presentationModel.ClickLine();
            Assert.AreEqual(_presentationModel.IsLine(), false);
        }

        [TestMethod()]
        public void IsRectangleTest()
        {
            _presentationModel.ClickRectangle();
            Assert.AreEqual(_presentationModel.IsRectangle(), false);
        }
        [TestMethod()]
        public void IsHexagonTest()
        {
            _presentationModel.ClickHexagon();
            Assert.AreEqual(_presentationModel.IsHexagon(), false);
        }
        [TestMethod()]
        public void SetCursorTest()
        {
            _presentationModel.SetCursor();
            Assert.AreEqual(_presentationModel.IsHexagon(), true);
            Assert.AreEqual(_presentationModel.IsLine(), true);
            Assert.AreEqual(_presentationModel.IsRectangle(), true);
        }
        [TestMethod()]
        public void GetSelectInfoTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            _presentationModel.Draw(_iGraphics);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(25, 25);
            Assert.AreEqual("Select:Line (25,25,50,50)", _presentationModel.GetSelectInfo());
            Model.Instance().Clear();
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().ReleasePointer(15, 15);
            Assert.AreEqual("Select:", _presentationModel.GetSelectInfo());
        }
    }
}