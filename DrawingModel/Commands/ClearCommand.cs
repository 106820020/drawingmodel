﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public class ClearCommand : ICommand
    {
        List<IShape> _shapes;
        public ClearCommand(List<IShape> shape)
        {
            _shapes = new List<IShape>(shape);
        }

        // exe
        public void Do()
        {
            Model.Instance().ClearShape();
        }

        // unexe
        public void Undo()
        {
            foreach (IShape shape in _shapes)
                Model.Instance().DrawShape(shape);
        }
    }
}
