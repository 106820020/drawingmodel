﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public class CombineCommand : ICommand
    {
        IShape _cs;
        List<IShape> _select;
        List<int> _index;

        public CombineCommand(IShape cs, List<IShape> select, List<int> index)
        {
            _cs = cs;
            _select = select;
            _index = index;
        }

        // execute command
        public void Do()
        {
            _cs.CombineShapes(_select);
            for (int i = 0; i < _select.Count; i++)
            {
                CommandManager.Instance().Execute(new DeleteCommand(_select[i], _index[i]));
                UpdateShapesIndex(_index[i]);
            }
            Model.Instance().DrawShape(_cs);
        }

        // unexecute command
        public void Undo()
        {
            Model.Instance().DeleteShape();
        }

        private void UpdateShapesIndex(int index)
        {
            for (int i = 0; i < _select.Count; i++)
            {
                if (_index[i] > index)
                    _index[i] -= 1;
            }
        }
    }
}
