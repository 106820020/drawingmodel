﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public class CommandManager
    {
        static CommandManager _commandManager;
        Stack<ICommand> _undo = new Stack<ICommand>();
        Stack<ICommand> _redo = new Stack<ICommand>();
        public static CommandManager Instance()
        {
            if (_commandManager == null)
                _commandManager = new CommandManager();
            return _commandManager;
        }

        private CommandManager()
        {

        }
        //exe
        public void Execute(ICommand command)
        {
            command.Do();
            _undo.Push(command);// push command 進 undo stack
            _redo.Clear();// 清除redo stack
        }

        // undo
        public void Undo()
        {
            ICommand command = _undo.Pop();
            _redo.Push(command);
            command.Undo();
        }

        // redo
        public void Redo()
        {
            ICommand command = _redo.Pop();
            _undo.Push(command);
            command.Do();
        }

        // retrun redo enable
        public bool IsRedoEnabled
        {
            get
            {
                return _redo.Count != 0;
            }
        }

        // return undo enable
        public bool IsUndoEnabled
        {
            get
            {
                return _undo.Count != 0;
            }
        }
    }
}
