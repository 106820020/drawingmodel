﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    class DeleteCommand : ICommand
    {
        IShape _shape;
        int _index;
        public DeleteCommand(IShape shape, int index)
        {
            _shape = shape;
            _index = index;
        }

        // execute command
        public void Do()
        {
            Model.Instance().DeleteShape(_index);
        }

        // unexecute command
        public void Undo()
        {
            Model.Instance().UnDeleteShape(_index, _shape);
        }
    }
}
