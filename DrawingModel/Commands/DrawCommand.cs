﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public class DrawCommand : ICommand
    {
        IShape _shape;
        public DrawCommand(IShape shape)
        {
            _shape = shape;
        }

        // execute command
        public void Do()
        {
            Model.Instance().DrawShape(_shape);
        }

        // unexecute command
        public void Undo()
        {
            Model.Instance().DeleteShape();
        }
    }
    
}
