﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public interface ICommand
    {
        // do
        void Do();
        
        //undo
        void Undo();
    }
    
}
