﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public class MoveCommand : ICommand
    {
        IShape _shape;
        IShape _newShape;
        int _index;
        public MoveCommand(IShape shape, IShape newShape, int index)
        {
            _shape = shape;
            _newShape = newShape;
            _index = index;
        }

        // execute command
        public void Do()
        {
            Model.Instance().MoveShape(_index, _newShape);
        }

        // unexecute command
        public void Undo()
        {
            Model.Instance().MoveShape(_index, _shape);
        }
    }
}
