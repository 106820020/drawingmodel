﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands
{
    public class ResizeCommand : ICommand
    {
        IShape _shape;
        IShape _newShape;
        int _index;
        public ResizeCommand(IShape shape, IShape newShape, int index)
        {
            _shape = shape;
            _newShape = newShape;
            _index = index;
        }

        // execute command
        public void Do()
        {
            Model.Instance().ResizeShape(_index, _newShape);
        }

        // unexecute command
        public void Undo()
        {
            Model.Instance().ResizeShape(_index, _shape);
        }
    }
}
