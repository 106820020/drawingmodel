﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Compound : IShape
    {
        List<IShape> _shapes = new List<IShape>();

        public string type = "Compound";
        public double X1
        {
            get; set;
        }

        public double Y1
        {
            get; set;
        }

        public double X2
        {
            get; set;
        }

        public double Y2
        {
            get; set;
        }

        public Compound()
        {
        }

        public Compound(IShape shape)
        {
            X1 = shape.X1;
            X2 = shape.X2;
            Y1 = shape.Y1;
            Y2 = shape.Y2;
            _shapes = shape.GetChild();
        }
        // draw shape
        public void Draw(IGraphics graphics)
        {
            graphics.DrawCompound(_shapes);
        }

        // draw the frame of shape
        public void DrawFrame(IGraphics graphics)
        {
            graphics.DrawRectangleFrame(X1, Y1, X2, Y2);
        }

        // if it contain
        public bool IsContain(double x, double y)
        {
            if (x >= X1 && x <= X2 && y >= Y1 && y <= Y2)
                return true;
            else
                return false;
        }

        public void MoveChild(double mx, double my)
        {
            foreach (IShape shape in _shapes)
            {
                if (shape.GetType() == typeof(Compound))
                {
                    shape.MoveChild(mx, my);
                }
                else
                {
                    shape.X1 += mx;
                    shape.X2 += mx;
                    shape.Y1 += my;
                    shape.Y2 += my;
                }
            }
        }

        public List<IShape> GetChild()
        {
            return _shapes;
        }

        public void Add(IShape shape)
        {
            _shapes.Add(shape);
        }

        public void CombineShapes(List<IShape> shapes)
        {
            foreach (IShape shape in shapes)
            {
                _shapes.Add(shape);
            }
            SetCompoundPosition();
        }

        private void SetCompoundPosition()
        {
            X1 = _shapes[0].X1;
            X2 = _shapes[0].X2;
            Y1 = _shapes[0].Y1;
            Y2 = _shapes[0].Y2;
            foreach (IShape shape in _shapes)
            {
                if (Math.Min(shape.X1, shape.X2) < X1)
                    X1 = Math.Min(shape.X1, shape.X2);
                if (Math.Max(shape.X1, shape.X2) > X2)
                    X2 = Math.Max(shape.X1, shape.X2);

                if (shape.GetType() == typeof(Hexagon))
                {
                    if (Math.Min(((shape.Y1 - shape.Y2) / 2 + shape.Y1) - 2, (shape.Y2 - (shape.Y1 - shape.Y2) / 2) - 2) < Y1)
                        Y1 = Math.Min(((shape.Y1 - shape.Y2) / 2 + shape.Y1) - 2, (shape.Y2 - (shape.Y1 - shape.Y2) / 2) - 2);
                    if (Math.Max(((shape.Y1 - shape.Y2) / 2 + shape.Y1) - 2, (shape.Y2 - (shape.Y1 - shape.Y2) / 2) - 2) > Y2)
                        Y2 = Math.Max(((shape.Y1 - shape.Y2) / 2 + shape.Y1) - 2, (shape.Y2 - (shape.Y1 - shape.Y2) / 2) - 2);
                }
                else
                {
                    if (Math.Min(shape.Y1, shape.Y2) < Y1)
                        Y1 = Math.Min(shape.Y1, shape.Y2);
                    if (Math.Max(shape.Y1, shape.Y2) > Y2)
                        Y2 = Math.Max(shape.Y1, shape.Y2);
                }
                
            }
        }

    }
}
