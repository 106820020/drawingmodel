﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Factory
    {
        // ishape
        public static IShape ConstructShape(String shape)
        {
            if (shape == "Line")
                return new Line();
            else if (shape == "Rectangle")
                return new Rectangle();
            else if (shape == "Hexagon")
                return new Hexagon();
            else if (shape == "Compound")
                return new Compound();
            else
                return null;
        }

        // ishape
        public static IShape CopyShape(IShape shape)
        {
            if (shape.GetType() == new Line().GetType())
                return new Line(shape);
            else if (shape.GetType() == new Rectangle().GetType())
                return new Rectangle(shape);
            else if (shape.GetType() == new Hexagon().GetType())
                return new Hexagon(shape);
            else if (shape.GetType() == new Compound().GetType())
                return new Compound(shape);
            else
                return null;
        }
    }
}
