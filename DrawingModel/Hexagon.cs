﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Hexagon : IShape
    {
        const int TWO = 2;
        double _x3;
        double _y3;

        public string type = "Hexagon";
        public double X1
        {
            get; set;
        }

        public double Y1
        {
            get; set;
        }

        public double X2
        {
            get; set;
        }

        public double Y2
        {
            get; set;
        }

        public Hexagon()
        {
        }
        public Hexagon(IShape shape)
        {
            X1 = shape.X1;
            X2 = shape.X2;
            Y1 = shape.Y1;
            Y2 = shape.Y2;
        }
        // draw shape
        public void Draw(IGraphics graphics)
        {
            graphics.DrawHexagon(X1, Y1, X2, Y2);
        }

        // draw the frame of shape
        public void DrawFrame(IGraphics graphics)
        {
            graphics.DrawHexagonFrame(X1, Y1, X2, Y2);
        }

        // if it contain
        public bool IsContain(double x, double y)
        {
            _x3 = X1;
            _y3 = Y1;
            Adjust();
            double y3 = Y1 + (x - X1) * (((Y1 - Y2) / TWO + Y1) - Y1) / ((X1 + X2) / TWO - X1);
            double y4 = Y1 + (x - X2) * (((Y1 - Y2) / TWO + Y1) - Y1) / ((X1 + X2) / TWO - X2);
            double y5 = Y2 + (x - X1) * ((Y2 - (Y1 - Y2) / TWO) - Y2) / ((X1 + X2) / TWO - X1);
            double y6 = Y2 + (x - X2) * ((Y2 - (Y1 - Y2) / TWO) - Y2) / ((X1 + X2) / TWO - X2);
            if (x <= X2 && X1 <= x && y > y3 && y > y4 && y < y5 && y < y6)
                return true;
            return false;
        }

        // adjust x1y1
        private void Adjust()
        {
            if (X1 > X2 && Y1 > Y2)
            {
                X1 = X2;
                Y1 = Y2;
                X2 = _x3;
                Y2 = _y3;
            }
            else if (X1 < X2 && Y1 > Y2)
            {
                Y1 = Y2;
                Y2 = _y3;
            }
            else if (X1 > X2 && Y1 < Y2)
            {
                X1 = X2;
                X2 = _x3;
            }
        }

        public void CombineShapes(List<IShape> shapes)
        {

        }

        public void Add(IShape shape)
        {

        }

        public List<IShape> GetChild()
        {
            return null;
        }

        public void MoveChild(double x, double y)
        {

        }
    }
}
