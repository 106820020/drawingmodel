﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public interface IGraphics
    {
        // clear all
        void ClearAll();

        // draw line
        void DrawLine(double x1, double y1, double x2, double y2);

        // draw the frame of line
        void DrawLineFrame(double x1, double y1, double x2, double y2);

        // draw rec
        void DrawRectangle(double x1, double y1, double x2, double y2);

        // draw the frame of rec
        void DrawRectangleFrame(double x1, double y1, double x2, double y2);

        // draw hexagon
        void DrawHexagon(double x1, double y1, double x2, double y2);

        // draw the frame of hexagon
        void DrawHexagonFrame(double x1, double y1, double x2, double y2);
        void DrawCompound(List<IShape> shapes);
    }
}
