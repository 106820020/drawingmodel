﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Line : IShape
    {
        const int SIX = 6;

        public string type = "Line";
        public double X1
        {
            get; set;
        }
        public double Y1
        {
            get; set;
        }
        public double X2
        {
            get; set;
        }

        public double Y2
        {
            get; set;
        }

        public Line()
        {

        }

        public Line(IShape shape)
        {
            X1 = shape.X1;
            X2 = shape.X2;
            Y1 = shape.Y1;
            Y2 = shape.Y2;
        }
        // draw shape
        public void Draw(IGraphics graphics)
        {
            graphics.DrawLine(X1, Y1, X2, Y2);
        }

        // draw the frame of shape
        public void DrawFrame(IGraphics graphics)
        {
            graphics.DrawLineFrame(X1, Y1, X2, Y2);
        }

        // if it contain
        public bool IsContain(double x, double y)
        {
            double y3 = Y1 + (x - X1) * (Y2 - Y1) / (X2 - X1);
            double x3 = X1 + (y - Y1) * (X2 - X1) / (Y2 - Y1);
            if ((Math.Abs(y - y3) < SIX || Math.Abs(x - x3) < SIX) && x <= Math.Max(X1, X2) + (int)3m && x >= Math.Min(X1, X2) - (int)3m)
                return true;
            return false;
        }

        public void CombineShapes(List<IShape> shapes)
        {

        }

        public void Add(IShape shape)
        {

        }

        public List<IShape> GetChild()
        {
            return null;
        }

        public void MoveChild(double x, double y)
        {

        }
    }
}
