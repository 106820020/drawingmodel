﻿using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawingModel.States;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace DrawingModel
{
    public enum Mode
    {
        Rectangle,
        Line,
        Hexagon
    }
    public class Model
    {
        static Model _model;
        public event ModelChangedEventHandler _modelChanged;
        public delegate void ModelChangedEventHandler();
        CursorState _cursorState;
        DrawState _drawState;
        MutiSelectState _mutiSelectState;
        ClearState _clearState;
        State _state;
        List<IShape> _shapes = new List<IShape>();
        List<IShape> _getSelectedShape = new List<IShape>();

        public static Model Instance()
        {
            if (_model == null)
                _model = new Model();
            return _model;
        }

        public Mode mode
        {
            get; set;
        }

        private Model()
        {
            CommandManager.Instance();
            _drawState = new DrawState();
            _cursorState = new CursorState();
            _mutiSelectState = new MutiSelectState();
            _clearState = new ClearState();
            _state = _cursorState;
        }

        // press pointer
        public void PressPointer(double x, double y)
        {
            _state.PressPointer(x, y);
        }

        // move pointer
        public void MovePointer(double x, double y)
        {
            _state.MovePointer(x, y);
            NotifyModelChanged();
        }

        // release pointer
        public void ReleasePointer(double x, double y)
        {
            _state.ReleasePointer(x, y);
            NotifyModelChanged();
        }

        // draw shape
        public void DrawShape(IShape shape)
        {
            _shapes.Add(shape);
        }

        //ResizeShape
        public void ResizeShape(int index, IShape shape)
        {
            _shapes[index] = shape;
        }

        public void MoveShape(int index, IShape shape)
        {
            _shapes[index] = shape;
        }

        // delete shape
        public void DeleteShape()
        {
            _shapes.RemoveAt(_shapes.Count - 1);
        }

        // delete shape
        public void DeleteShape(int index)
        {
            _shapes.RemoveAt(index);
        }

        // undelete shape
        public void UnDeleteShape(int index, IShape shape)
        {
            _shapes.Insert(index, shape);
        }

        public void Delete()
        {
            _state.DeleteShape();
            NotifyModelChanged();
        }
        // clear shape
        public void ClearShape()
        {
            _shapes.Clear();
        }

        // clear
        public void Clear()
        {
            _state.Clear();
            NotifyModelChanged();
        }

        // undo
        public void Undo()
        {
            CommandManager.Instance().Undo();
            _state.Undo();
            NotifyModelChanged();
        }

        // redo
        public void Redo()
        {
            CommandManager.Instance().Redo();
            _state.Redo();
            NotifyModelChanged();
        }

        // draw
        public void Draw(IGraphics graphics)
        {
            graphics.ClearAll();
            foreach (IShape shapes in _shapes)
                shapes.Draw(graphics);
            _state.Draw(graphics);
        }

        // change notifymodel
        void NotifyModelChanged()
        {
            if (_modelChanged != null)
                _modelChanged();
        }

        public void SetState(State state)
        {
            _state.Init();
            _state = state;
        }

        //SetCursorState
        public State GetCursorState()
        {
            return _cursorState;
        }

        public State GetDrawState()
        {
            return _drawState;
        }

        //SetMutiSelectState
        public State GetMutiSelectState()
        {
            return _mutiSelectState;
        }

        //SetClearState
        public State GetClearState()
        {
            return _clearState;
        }

        public bool IsRedo
        {
            get
            {
                return CommandManager.Instance().IsRedoEnabled;
            }
        }

        //SetCursorState
        public bool IsUndo
        {
            get
            {
                return CommandManager.Instance().IsUndoEnabled;
            }
        }

        //SetCursorState
        public State GetState()
        {
            return _state;
        }

        //get shapes
        public IShape GetSelectShape()
        {
            return _state.GetShape();
        }

        // click button
        public void ClickButton()
        {
            _state.ClickButton();
            NotifyModelChanged();
        }

        // get shapes
        public List<IShape> GetShapes()
        {
            return _shapes;
        }

        public void ClickCombine()
        {
            _state.Combine();
        }

        //import file from json
        public void ImportFile(string path)
        {
            ShapeParser parser = new ShapeParser();
            parser.parse(path);
            _shapes = parser.getResult();
        }

        //export list of shape to json file
        public void ExportFile(string path)
        {
            JArray outputShapes = new JArray();

            foreach (IShape shape in _shapes)
            {
                if (shape.GetType() == typeof(Line))
                {
                    outputShapes.Add(JToken.FromObject((Line)shape));
                }
                else if (shape.GetType() == typeof(Rectangle))
                {
                    outputShapes.Add(JToken.FromObject((Rectangle)shape));
                }
                else if (shape.GetType() == typeof(Hexagon))
                {
                    outputShapes.Add(JToken.FromObject((Hexagon)shape));
                }
                else if (shape.GetType() == typeof(Compound))
                {
                    JObject jObject = JObject.FromObject((Compound)shape);
                    List<IShape> insideShapes = shape.GetChild();
                    jObject.Add("content",jsonConverter(insideShapes));
                    outputShapes.Add(jObject);
                }

            }

            string output = JsonConvert.SerializeObject(outputShapes, Formatting.Indented);
            System.IO.File.WriteAllText(path, output);
        }

        private JArray jsonConverter(List<IShape> listOfShapes)
        {
            JArray result = new JArray();
            foreach(IShape shape in listOfShapes)
            {
                if (shape.GetType() == typeof(Line))
                {
                    result.Add(JToken.FromObject((Line)shape));
                }
                else if (shape.GetType() == typeof(Rectangle))
                {
                    result.Add(JToken.FromObject((Rectangle)shape));
                }
                else if (shape.GetType() == typeof(Hexagon))
                {
                    result.Add(JToken.FromObject((Hexagon)shape));
                }
            }

            return result;
        }

        public bool ClickShapes(double x, double y)
        {
            foreach (IShape shape in _shapes)
                if (shape.IsContain(x, y))
                    return true;
            return false;
        }
    }
}