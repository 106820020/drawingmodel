﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class Rectangle : IShape
    {
        double _x3;
        double _y3;

        public string type = "Rectangle";
        public double X1
        {
            get; set;
        }

        public double Y1
        {
            get; set;
        }

        public double X2
        {
            get; set;
        }

        public double Y2
        {
            get; set;
        }

        public Rectangle()
        {
        }
        public Rectangle(IShape shape)
        {
            X1 = shape.X1;
            X2 = shape.X2;
            Y1 = shape.Y1;
            Y2 = shape.Y2;
        }
        // draw shape
        public void Draw(IGraphics graphics)
        {
            graphics.DrawRectangle(X1, Y1, X2, Y2);
        }

        // draw the frame of shape
        public void DrawFrame(IGraphics graphics)
        {
            graphics.DrawRectangleFrame(X1, Y1, X2, Y2);
        }

        // if it contain
        public bool IsContain(double x, double y)
        {
            _x3 = X1;
            _y3 = Y1;
            Adjust();
            if (x >= Math.Min(X1, X2) && x <= Math.Max(X1, X2) && y >= Math.Min(Y1, Y2) && y <= Math.Max(Y1, Y2))
                return true;
            return false;
        }

        // adjust x1y1
        private void Adjust()
        {
            if (X1 > X2 && Y1 > Y2)
            {
                X1 = X2;
                Y1 = Y2;
                X2 = _x3;
                Y2 = _y3;
            }
            else if (X1 < X2 && Y1 > Y2)
            {
                Y1 = Y2;
                Y2 = _y3;
            }
            else if (X1 > X2 && Y1 < Y2)
            {
                X1 = X2;
                X2 = _x3;
            }
        }

        public void CombineShapes(List<IShape> shapes)
        {

        }

        public void Add(IShape shape)
        {

        }

        public List<IShape> GetChild()
        {
            return null;
        }

        public void MoveChild(double x, double y)
        {

        }
    }
}
