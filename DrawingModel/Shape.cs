﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public interface IShape
    {
        double X1
        {
            get; set;
        }

        double Y1
        {
            get; set;
        }

        double X2
        {
            get; set;
        }

        double Y2
        {
            get; set;
        }

        // draw shape
        void Draw(IGraphics graphics);

        // draw the frame of shape
        void DrawFrame(IGraphics graphics);
        
        // if it is contain
        bool IsContain(double x, double y);

        void CombineShapes(List<IShape> shapes);
        
        void Add(IShape shape);

        List<IShape> GetChild();

        void MoveChild(double x, double y);
    }
}
