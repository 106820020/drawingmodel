﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel
{
    public class ShapeBuilder
    {
        private IShape _shape;
        private Stack<Compound> _stack = new Stack<Compound>();

        public void BuildLine(double x1, double y1, double x2, double y2)
        {
            IShape sp = Factory.ConstructShape("Line");
            sp.X1 = x1;
            sp.Y1 = y1;
            sp.X2 = x2;
            sp.Y2 = y2;
            if (_stack.Count() == 0)
                this._shape = sp;
            else
                this._stack.Peek().Add(sp);

        }

        public void BuildRectangle(double x1, double y1, double x2, double y2)
        {
            IShape sp = Factory.ConstructShape("Rectangle");
            sp.X1 = x1;
            sp.Y1 = y1;
            sp.X2 = x2;
            sp.Y2 = y2;
            if (_stack.Count() == 0)
                this._shape = sp;
            else
                this._stack.Peek().Add(sp);
        }

        public void BuildHexagon(double x1, double y1, double x2, double y2)
        {
            IShape sp = Factory.ConstructShape("Hexagon");
            sp.X1 = x1;
            sp.Y1 = y1;
            sp.X2 = x2;
            sp.Y2 = y2;
            if (_stack.Count() == 0)
                this._shape = sp;
            else
                this._stack.Peek().Add(sp);
        }

        public void BeginBuildCompound(double x1, double y1, double x2, double y2)
        {
            IShape it = Factory.ConstructShape("Compound");
            it.X1 = x1;
            it.Y1 = y1;
            it.X2 = x2;
            it.Y2 = y2;
            _stack.Push((Compound)it);
        }

        public void EndBuildCompound()
        {
            IShape currentShape = _stack.Pop();
            if (_stack.Count() == 0)
                this._shape = currentShape;
            else
            {
                _stack.Peek().Add(currentShape);
            }

        }

        public IShape GetResult()
        {
            return this._shape;
        }


    }
}
