﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DrawingModel
{
    public class ShapeParser
    {
        private ShapeBuilder _builder;
        private List<IShape> _shapes;

        public ShapeParser()
        {
            _builder = new ShapeBuilder();
            _shapes = new List<IShape>();
        }

        public void parse(string path)
        {
            JArray o1 = JArray.Parse(File.ReadAllText(path));
            Console.WriteLine(o1.Count);
            foreach(JToken token in o1)
            {
                parseItem(token);

                _shapes.Add(_builder.GetResult());
            }
        }

        private void parseItem(JToken token)
        {
            switch (token["type"].ToString())
            {
                case "Line":
                    _builder.BuildLine((double)token["X1"], (double)token["Y1"], (double)token["X2"], (double)token["Y2"]);
                    break;
                case "Rectangle":
                    _builder.BuildRectangle((double)token["X1"], (double)token["Y1"], (double)token["X2"], (double)token["Y2"]);
                    break;
                case "Hexagon":
                    _builder.BuildHexagon((double)token["X1"], (double)token["Y1"], (double)token["X2"], (double)token["Y2"]);
                    break;
                case "Compound":
                    _builder.BeginBuildCompound((double)token["X1"], (double)token["Y1"], (double)token["X2"], (double)token["Y2"]);
                    foreach (JToken t in token["content"])
                        parseItem(t);
                    _builder.EndBuildCompound();
                    break;
            }
        }

        public List<IShape> getResult()
        {
            return _shapes;
        }
    }
}
