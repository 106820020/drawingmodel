﻿using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.States
{
    class ClearState : State
    {
        CursorState _cursorState;
        IShape _select;
        IShape _old;
        bool _isPressed = false;
        int _index;
        int mode; // 1 = move & 0 = resize
        const int TEN = 10;
        double px;
        double py;

        public ClearState()
        {
            _cursorState = new CursorState();
        }

        public void PressPointer(double x, double y)
        {
            _cursorState.PressPointer(x, y);
        }

        // move
        public void MovePointer(double x, double y)
        {
            _cursorState.MovePointer(x, y);
        }

        //release
        public void ReleasePointer(double x, double y)
        {
            _cursorState.ReleasePointer(x, y);
        }

        //clear
        public void Clear()
        {
            _cursorState.Clear();
        }

        //undo
        public void Undo()
        {
            _cursorState.Undo();
        }

        //redo
        public void Redo()
        {
            _cursorState.Redo();
        }

        //draw
        public void Draw(IGraphics graphics)
        {
            _cursorState.Draw(graphics);
        }

        //getshape
        public IShape GetShape()
        {
            return _cursorState.GetShape();
        }

        //click button
        public void ClickButton()
        {
            _cursorState.ClickButton();
        }

        //init
        public void Init()
        {
            _cursorState.Init();
        }

        public void DeleteShape()
        {
            _cursorState.DeleteShape();
        }

        public void Combine()
        {
            _cursorState.Combine();
        }
    }
}