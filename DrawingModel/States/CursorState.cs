﻿using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.States
{
    public class CursorState : State
    {
        IShape _select;
        IShape _old;
        bool _isPressed = false;
        int _index;
        int mode; // 1 = move & 0 = resize
        const int TEN = 10;
        double px;
        double py;

        public CursorState()
        {
        }

        public void PressPointer(double x, double y)
        {
            if (_select != null && _select.X2 - TEN < x && x < _select.X2 + TEN && _select.Y2 - TEN < y && y < _select.Y2 + TEN)
            {
                _isPressed = true;
                mode = 0;
            }
            else if (_select != null && x < Math.Max(_select.X1, _select.X2) && x > Math.Min(_select.X1, _select.X2) && y > Math.Min(_select.Y1, _select.Y2) && y < Math.Max(_select.Y1, _select.Y2))
            {
                _isPressed = true;
                mode = 1;
                px = x;
                py = y;
            }
            else
            {
                _select = null;
                foreach (IShape shape in Model.Instance().GetShapes())
                    if (shape.IsContain(x, y))
                    {
                        _select = Factory.CopyShape(shape);
                        _index = Model.Instance().GetShapes().IndexOf(shape);
                        _old = Factory.CopyShape(shape);
                    }

            }
        }

        // move
        public void MovePointer(double x, double y)
        {
            if (_isPressed && mode == 0)
            {
                _select.X2 = x;
                _select.Y2 = y;
            }
            else if(_isPressed && mode == 1)
            {
                double mx = x - px;
                double my = y - py;
                _select.X1 += mx;
                _select.X2 += mx;
                _select.Y1 += my;
                _select.Y2 += my;
                _select.MoveChild(mx, my);
                px = x;
                py = y;
            }
        }

        //release
        public void ReleasePointer(double x, double y)
        {
            if (_isPressed && mode == 0)
            {
                _isPressed = false;
                _select.X2 = x;
                _select.Y2 = y;
                CommandManager.Instance().Execute(new ResizeCommand(_old, _select, _index));
            }
            if (_isPressed && mode == 1)
            {
                _isPressed = false;
                double mx = x - px;
                double my = y - py;
                _select.X1 += mx;
                _select.X2 += mx;
                _select.Y1 += my;
                _select.Y2 += my;
                CommandManager.Instance().Execute(new MoveCommand(_old, _select, _index));
            }
        }

        //clear
        public void Clear()
        {
            _select = null;
            CommandManager.Instance().Execute(new ClearCommand(Model.Instance().GetShapes()));

        }
        //clear
        public void DeleteShape()
        {
            if(_select != null)
            {
                CommandManager.Instance().Execute(new DeleteCommand(_select, _index));
                _select = null;
            }
        }

            //undo
        public void Undo()
        {
            _select = null;
        }

        //redo
        public void Redo()
        {
            _select = null;
        }

        //draw
        public void Draw(IGraphics graphics)
        {
            if (_select != null)
                _select.DrawFrame(graphics);
        }

        //getshape
        public IShape GetShape()
        {
            return _select;
        }

        //click button
        public void ClickButton()
        {
            _select = null;
        }

        //init
        public void Init()
        {
            _select = null;
        }

        public void Combine()
        {

        }
    }
}

