﻿using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.States
{
    public class DrawState : State
    {
        IShape _hint;
        double _firstPointX;
        double _firstPointY;
        bool _isPressed = false;

        public DrawState()
        {
        }

        // press pointer
        public void PressPointer(double x, double y)
        {
            if (x > 0 && y > 0)
            {
                _firstPointX = x;
                _firstPointY = y;
                ConstructShape();
                _isPressed = true;
            }
        }

        //move
        public void MovePointer(double x, double y)
        {
            if (_isPressed)
            {
                _hint.X2 = x;
                _hint.Y2 = y;
            }
        }

        //release
        public void ReleasePointer(double x, double y)
        {
            if (_isPressed)
            {
                _isPressed = false;
                _hint.X1 = _firstPointX;
                _hint.Y1 = _firstPointY;
                _hint.X2 = x;
                _hint.Y2 = y;
                CommandManager.Instance().Execute(new DrawCommand(_hint));
            }
        }

        //call factory
        private void ConstructShape()
        {
            _hint = Factory.ConstructShape(Model.Instance().mode.ToString());
            _hint.X1 = _firstPointX;
            _hint.Y1 = _firstPointY;
        }

        //clear
        public void Clear()
        {
        }

        //undo
        public void Undo()
        {
            _hint = null;
        }

        //redo
        public void Redo()
        {
            _hint = null;
        }

        //draw
        public void Draw(IGraphics graphics)
        {
            if (_isPressed)
                _hint.Draw(graphics);
        }

        //get
        public IShape GetShape()
        {
            return _hint;
        }

        //click
        public void ClickButton()
        {
            _hint = null;
        }

        //init
        public void Init()
        {
            _hint = null;
        }
        public void DeleteShape()
        {

        }

        public void Combine()
        {

        }
    }
}
