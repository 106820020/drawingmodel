﻿using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.States
{
    class MutiSelectState : State
    {
        List<IShape> _select;
        List<IShape> _old;
        List<int> _index;
        bool _isPressed = false;
        int mode; // 1 = move & 0 = resize
        double px;
        double py;

        public MutiSelectState()
        {
            _select = new List<IShape>();
            _old = new List<IShape>();
            _index = new List<int>();
        }

        public void PressPointer(double x, double y)
        {
            if (_select != null)
            {
                foreach (IShape shape in _select)
                {
                    if (shape.X2 - 10 < x && x < shape.X2 + 10 && shape.Y2 - 10 < y && y < shape.Y2 + 10)
                    {
                        _isPressed = true;
                        mode = 0;
                        px = x;
                        py = y;
                        break;
                    }
                    else if (shape.IsContain(x, y))
                    {
                        _isPressed = true;
                        mode = 1;
                        px = x;
                        py = y;
                        break;
                    }
                }
            }
            if (!_isPressed)
            {
                bool clickOnCanvas = true;
                foreach (IShape shape in Model.Instance().GetShapes())
                    if (shape.IsContain(x, y))
                    {
                        _select.Add(Factory.CopyShape(shape));
                        _index.Add(Model.Instance().GetShapes().IndexOf(shape));
                        _old.Add(Factory.CopyShape(shape));
                        clickOnCanvas = false;
                    }

                if (clickOnCanvas)
                {
                    _select.Clear();
                    _index.Clear();
                    _old.Clear();
                }
            }
        }

        // move
        public void MovePointer(double x, double y)
        {
            double mx = x - px;
            double my = y - py;

            if (_isPressed && mode == 0)
            {
                foreach (IShape shape in _select)
                {
                    shape.X2 += mx;
                    shape.Y2 += my;
                }
                px = x;
                py = y;
            }
            else if (_isPressed && mode == 1)
            {
               
                foreach (IShape shape in _select)
                {
                    shape.X1 += mx;
                    shape.X2 += mx;
                    shape.Y1 += my;
                    shape.Y2 += my;
                    shape.MoveChild(mx, my);
                }
                px = x;
                py = y;
            }
        }

        //release
        public void ReleasePointer(double x, double y)
        {
            double mx = x - px;
            double my = y - py;

            if (_isPressed && mode == 0)
            {
                _isPressed = false;
                foreach (IShape shape in _select)
                {
                    shape.X2 += mx;
                    shape.Y2 += my;
                    CommandManager.Instance().Execute(new ResizeCommand(_old[_select.IndexOf(shape)], shape, _index[_select.IndexOf(shape)]));
                }
            }
            else if (_isPressed && mode == 1)
            {
                _isPressed = false;
                foreach (IShape shape in _select)
                {
                    shape.X1 += mx;
                    shape.X2 += mx;
                    shape.Y1 += my;
                    shape.Y2 += my;
                    CommandManager.Instance().Execute(new MoveCommand(_old[_select.IndexOf(shape)], shape, _index[_select.IndexOf(shape)]));
                }
                px = x;
                py = y;
            }
        }

        //clear
        public void Clear()
        {
            _select.Clear();
            _index.Clear();
            _old.Clear();
            CommandManager.Instance().Execute(new ClearCommand(Model.Instance().GetShapes()));
        }

        //undo
        public void Undo()
        {
            _select.Clear();
            _index.Clear();
            _old.Clear();
        }

        //redo
        public void Redo()
        {
            _select.Clear();
            _index.Clear();
            _old.Clear();
        }

        //draw
        public void Draw(IGraphics graphics)
        {
            foreach (IShape shape in _select)
            {
                shape.DrawFrame(graphics);
            }
        }

        //click button
        public void ClickButton()
        {
            _select.Clear();
            _index.Clear();
            _old.Clear();
        }

        //init
        public void Init()
        {
            _select.Clear();
            _index.Clear();
            _old.Clear();
        }

        //getshape
        public IShape GetShape()
        {
            return Factory.ConstructShape("Compound");
        }

        //clear
        public void DeleteShape()
        {
            if (_select != null)
            {
                for (int i = 0; i < _select.Count; i++)
                {
                    CommandManager.Instance().Execute(new DeleteCommand(_select[i], _index[i]));
                    UpdateShapesIndex(_index[i]);
                }
                _select.Clear();
                _index.Clear();
                _old.Clear();
            }
        }

        private void UpdateShapesIndex(int index)
        {
            for (int i = 0; i < _select.Count; i++)
            {
                if (_index[i] > index)
                    _index[i] -= 1;
            }
        }

        public void Combine()
        {
            IShape cs = Factory.ConstructShape("Compound");
            CommandManager.Instance().Execute(new CombineCommand(cs, _select, _index));
            _select.Clear();
            _index.Clear();
            _old.Clear();
        }
    }
}
