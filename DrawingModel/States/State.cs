﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.States
{
    public interface State
    {
        //public abstract string GetState();
        void PressPointer(double x, double y);
        //move
        void MovePointer(double x, double y);
        //release
        void ReleasePointer(double x, double y);
        //clear
        void Clear();
        //undo
        void Undo();
        //redo
        void Redo();
        //draw
        void Draw(IGraphics graphics);
        //click
        void ClickButton();
        //init
        void Init();
        //get
        IShape GetShape();

        void DeleteShape();

        void Combine();
    }
}
