﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands.Tests
{
    [TestClass()]
    public class CommandManagerTests
    {
        ICommand _command;

        [TestInitialize]
        public void Initialize()
        {
            Line line = new Line();
            _command = new DrawCommand(line);
            Model.Instance().ClearShape();
        }
        [TestMethod()]
        public void ExecuteTest()
        {
            CommandManager.Instance().Execute(_command);
            Assert.AreEqual(1, Model.Instance().GetShapes().Count);
        }

        [TestMethod()]
        public void UndoTest()
        {
            CommandManager.Instance().Execute(_command);
            ICommand command = new ClearCommand(Model.Instance().GetShapes());
            CommandManager.Instance().Execute(command);
            CommandManager.Instance().Undo();
            Assert.AreEqual(1, Model.Instance().GetShapes().Count);
        }

        [TestMethod()]
        public void RedoTest()
        {
            CommandManager.Instance().Execute(_command);
            ICommand command = new ClearCommand(Model.Instance().GetShapes());
            CommandManager.Instance().Execute(command);
            CommandManager.Instance().Undo();
            CommandManager.Instance().Redo();
            Assert.AreEqual(0, Model.Instance().GetShapes().Count);
        }
    }
}