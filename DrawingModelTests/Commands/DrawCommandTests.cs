﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Commands.Tests
{
    [TestClass()]
    public class DrawCommandTests
    {
        DrawCommand _drawCommand;
        IShape _shape;

        [TestInitialize]
        public void Initialize()
        {
            _shape = new Line();
            _drawCommand = new DrawCommand(_shape);
        }
        [TestMethod()]
        public void ExecuteTest()
        {
            _drawCommand.Do();
            Assert.IsNotNull(_drawCommand);
        }

        [TestMethod()]
        public void UnExecuteTest()
        {
            _drawCommand.Do();
            _drawCommand.Undo();
            Assert.IsNotNull(_drawCommand);
        }
    }
}