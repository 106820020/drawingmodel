﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class FactoryTests
    {
        [TestMethod()]
        public void ConstructShapeTest()
        {
            Assert.AreEqual(new Hexagon().GetType(), Factory.ConstructShape("Hexagon").GetType());
            Assert.AreEqual(new Line().GetType(), Factory.ConstructShape("Line").GetType());
            Assert.AreEqual(new Rectangle().GetType(), Factory.ConstructShape("Rectangle").GetType());
        }
        [TestMethod()]
        public void CopyShapeTest()
        {
            IShape line = new Line();
            IShape rectangle = new Rectangle();
            IShape hexagon = new Hexagon();
            Assert.AreEqual(new Hexagon().GetType(), Factory.CopyShape(hexagon).GetType());
            Assert.AreEqual(new Line().GetType(), Factory.CopyShape(line).GetType());
            Assert.AreEqual(new Rectangle().GetType(), Factory.CopyShape(rectangle).GetType());
        }
    }
}