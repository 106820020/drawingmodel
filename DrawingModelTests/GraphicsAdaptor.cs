﻿using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    class GraphicsAdaptor : IGraphics
    {
        public double X1
        {
            get;set;
        }

        public double Y1
        {
            get;set;
        }

        public double X2
        {
            get;set;
        }

        public double Y2
        {
            get;set;
        }
        public GraphicsAdaptor()
        {

        }

        public void ClearAll()
        {

        }

        public void DrawLine(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }
        public void DrawLineFrame(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }

        public void DrawRectangle(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }
        public void DrawRectangleFrame(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }
        public void DrawHexagon(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }
        public void DrawHexagonFrame(double x1, double y1, double x2, double y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y1;
            Y2 = y2;
        }

        public void DrawCompound(List<IShape> shapes)
        {
            foreach (IShape shape in shapes)
            {
                if (shape.GetType() == typeof(DrawingModel.Rectangle))
                    DrawRectangle(shape.X1, shape.Y1, shape.X2, shape.Y2);
                else if (shape.GetType() == typeof(DrawingModel.Line))
                    DrawLine(shape.X1, shape.Y1, shape.X2, shape.Y2);
                else if (shape.GetType() == typeof(DrawingModel.Hexagon))
                    DrawHexagon(shape.X1, shape.Y1, shape.X2, shape.Y2);
                else if (shape.GetType() == typeof(DrawingModel.Compound))
                    DrawCompound(shape.GetChild());
            }
        }
    }
}
