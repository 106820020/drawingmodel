﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class HexagonTests
    {
        Hexagon _hexagon;
        GraphicsAdaptor _iGraphics;
        [TestInitialize]
        public void InInitializer()
        {
            _hexagon = new Hexagon();
            _iGraphics = new GraphicsAdaptor();
        }
        [TestMethod()]
        public void DrawTest()
        {
            _hexagon.X1 = 10;
            _hexagon.X2 = 20;
            _hexagon.Y1 = 10;
            _hexagon.Y2 = 20;
            _hexagon.Draw(_iGraphics);
            Assert.AreEqual(10, _iGraphics.X1);
            Assert.AreEqual(10, _iGraphics.Y1);
            Assert.AreEqual(20, _iGraphics.X2);
            Assert.AreEqual(20, _iGraphics.Y2);
        }

        [TestMethod()]
        public void DrawFrameTest()
        {
            _hexagon.X1 = 10;
            _hexagon.X2 = 20;
            _hexagon.Y1 = 10;
            _hexagon.Y2 = 20;
            _hexagon.DrawFrame(_iGraphics);
            Assert.AreEqual(10, _iGraphics.X1);
            Assert.AreEqual(10, _iGraphics.Y1);
            Assert.AreEqual(20, _iGraphics.X2);
            Assert.AreEqual(20, _iGraphics.Y2);
        }

        [TestMethod()]
        public void IsContainTest()
        {
            _hexagon.X1 = 10;
            _hexagon.X2 = 20;
            _hexagon.Y1 = 10;
            _hexagon.Y2 = 20;
            Assert.IsTrue(_hexagon.IsContain(15, 15));
            _hexagon.X1 = 20;
            _hexagon.X2 = 10;
            _hexagon.Y1 = 20;
            _hexagon.Y2 = 10;
            Assert.IsTrue(_hexagon.IsContain(14, 13));
            _hexagon.X1 = 10;
            _hexagon.X2 = 5;
            _hexagon.Y1 = 10;
            _hexagon.Y2 = 20;
            Assert.IsFalse(_hexagon.IsContain(1, 1));
            _hexagon.X1 = 10;
            _hexagon.X2 = 20;
            _hexagon.Y1 = 10;
            _hexagon.Y2 = 5;
            Assert.IsFalse(_hexagon.IsContain(100, 100));
        }
        [TestMethod()]
        public void ShapeTest()
        {
            IShape hexagon = new Hexagon();
            Assert.AreEqual(new Hexagon().GetType(), Factory.CopyShape(hexagon).GetType());
        }
    }
}