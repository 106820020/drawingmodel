﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class LineTests
    {
        GraphicsAdaptor _iGraphics;

        //initialize
        [TestInitialize]
        public void Initrialize()
        {
            _iGraphics = new GraphicsAdaptor();
        }

        //ut
        [TestMethod()]
        public void DrawTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(20, _iGraphics.X1);
            Assert.AreEqual(20, _iGraphics.Y1);
            Assert.AreEqual(10, _iGraphics.X2);
            Assert.AreEqual(10, _iGraphics.Y2);
            Model.Instance().ReleasePointer(15, 15);
        }

        //ut
        [TestMethod()]
        public void DrawLineFrameTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().ReleasePointer(15, 15);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().ReleasePointer(20, 20);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(20, _iGraphics.X1);
            Assert.AreEqual(20, _iGraphics.Y1);
            Assert.AreEqual(15, _iGraphics.X2);
            Assert.AreEqual(15, _iGraphics.Y2);
            
        }

        //ut
        [TestMethod()]
        public void IsContainTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().ReleasePointer(15, 15);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(20, _iGraphics.X1);
            Assert.AreEqual(20, _iGraphics.Y1);
            Assert.AreEqual(15, _iGraphics.X2);
            Assert.AreEqual(15, _iGraphics.Y2);
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().ReleasePointer(25, 15);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().ReleasePointer(15, 25);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().ReleasePointer(40, 40);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().ReleasePointer(20, 20);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(100, 100);
            Model.Instance().ReleasePointer(100, 100);
            Model.Instance().Draw(_iGraphics);
        }
        [TestMethod()]
        public void ShapeTest()
        {
            IShape line = new Line();
            Assert.AreEqual(new Line().GetType(), Factory.CopyShape(line).GetType());
        }
    }
}