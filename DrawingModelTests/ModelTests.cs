﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawingModel.States;
using DrawingModel.Commands;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class ModelTests
    {
        GraphicsAdaptor _iGraphics;

        // initialize
        [TestInitialize]
        public void Initialize()
        {
            _iGraphics = new GraphicsAdaptor();
            Model.Instance()._modelChanged += this.EventHandler;
        }

        // test
        [TestMethod()]
        public void PointerPressedTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Assert.AreEqual(Model.Instance().GetState().GetType(), new DrawState().GetType());
            Initialize();
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(10, 10);
            Assert.AreEqual(Model.Instance().GetState().GetType(), new CursorState().GetType());
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Assert.AreEqual(Model.Instance().GetState().GetType(), new DrawState().GetType());
            Initialize();
            Model.Instance().mode = Mode.Hexagon;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Assert.AreEqual(Model.Instance().GetState().GetType(), new DrawState().GetType());
            Initialize();
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(-10, 10);
            Assert.AreEqual(Model.Instance().GetState().GetType(), new DrawState().GetType());
        }

        // test
        [TestMethod()]
        public void GetShapeTest()
        {
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Model.Instance().MovePointer(20, 30);
            Assert.AreEqual(Model.Instance().GetSelectShape().X2, 20);
        }

        // test
        [TestMethod()]
        public void PointerMovedTest()
        {
            Model.Instance().MovePointer(19, 20);
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Model.Instance().MovePointer(20, 30);
            Model.Instance().ReleasePointer(20, 30);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(20, 30);
            Assert.AreEqual(Model.Instance().GetSelectShape().X2, 20);
            Assert.AreEqual(Model.Instance().GetSelectShape().Y2, 30);
            Model.Instance().PressPointer(20, 30);
            Model.Instance().MovePointer(40,50);
            Model.Instance().ReleasePointer(50, 60);
            Model.Instance().Undo();
            Model.Instance().Redo();
        }

        // test
        [TestMethod()]
        public void PointerReleasedTest()
        {
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Model.Instance().MovePointer(20, 30);
            Model.Instance().ReleasePointer(15, 45);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Assert.AreEqual(Model.Instance().GetState().GetType(), new CursorState().GetType());
            Model.Instance().Clear();
            Model.Instance().Undo();
            Model.Instance().Redo();
            Model.Instance().ClickButton();
        }

        // test
        [TestMethod()]
        public void ClearTest()
        {
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(10, 10);
            Model.Instance().MovePointer(20, 30);
            Model.Instance().ReleasePointer(15, 45);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().Clear();
            Assert.AreEqual(Model.Instance().GetState().GetType(), new CursorState().GetType());
        }

        // test
        [TestMethod()]
        public void DrawTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().MovePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(25, _iGraphics.X1);
            Assert.AreEqual(25, _iGraphics.Y1);
            Assert.AreEqual(50, _iGraphics.X2);
            Assert.AreEqual(50, _iGraphics.Y2);
            Model.Instance().ReleasePointer(40, 40);
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(20, 20);
            Model.Instance().MovePointer(10, 10);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(20, _iGraphics.X1);
            Assert.AreEqual(20, _iGraphics.Y1);
            Assert.AreEqual(10, _iGraphics.X2);
            Assert.AreEqual(10, _iGraphics.Y2);
            Model.Instance().ReleasePointer(15, 15);
        }

        // test
        void EventHandler()
        {

        }

        [TestMethod()]
        public void RedoTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().Draw(_iGraphics);
            Model.Instance().Clear();
            Model.Instance().Undo();
            Model.Instance().Redo();
            Assert.AreEqual(Model.Instance().GetState().GetType(), new CursorState().GetType());
        }
        [TestMethod()]
        public void UndoTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().Draw(_iGraphics);
            Model.Instance().Clear();
            Model.Instance().Undo();
            Assert.AreEqual(Model.Instance().GetState().GetType(), new CursorState().GetType());
        }
        [TestMethod()]
        public void GetSelectShapeTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(25, 25);
            Assert.AreEqual(Model.Instance().GetSelectShape().X1, 25);
        }
        [TestMethod()]
        public void ClickButtonTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().ClickButton();
            Assert.AreEqual(Model.Instance().GetSelectShape(), null);
        }
        [TestMethod()]
        public void IsRedoTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(Model.Instance().IsRedo, false);
        }

        [TestMethod()]
        public void IsUndoTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().Clear();
            Assert.AreEqual(Model.Instance().IsUndo, true);
        }
        [TestMethod()]
        public void DrawShapeTest()
        {
            Model.Instance().ClearShape();
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().SetState(Model.Instance().GetCursorState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(25, 25);
            Model.Instance().DrawShape(Model.Instance().GetSelectShape());
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(Model.Instance().GetShapes().Count, 2);
        }
        [TestMethod()]
        public void DeleteShapeTest()
        {
            Model.Instance().ClearShape();
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().DeleteShape();
            Assert.AreEqual(Model.Instance().GetShapes().Count, 0);
        }
        [TestMethod()]
        public void ClearShapeTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Model.Instance().ClearShape();
            Assert.AreEqual(Model.Instance().GetShapes().Count, 0);
        }
        [TestMethod()]
        public void GetShapesTest()
        {
            Model.Instance().mode = Mode.Line;
            Model.Instance().SetState(Model.Instance().GetDrawState());
            Model.Instance().PressPointer(25, 25);
            Model.Instance().ReleasePointer(50, 50);
            Model.Instance().Draw(_iGraphics);
            Assert.AreEqual(Model.Instance().GetShapes().Count, 1);
        }
    }
}