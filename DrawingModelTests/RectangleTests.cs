﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingModel.Tests
{
    [TestClass()]
    public class RectangleTests
    {
        Rectangle _rectangle;
        GraphicsAdaptor _iGraphics;
        [TestInitialize]
        public void Initrialize()
        {
            _rectangle = new Rectangle();
            _iGraphics = new GraphicsAdaptor();
        }
        [TestMethod()]
        public void DrawTest()
        {
            _rectangle.X1 = 10;
            _rectangle.X2 = 20;
            _rectangle.Y1 = 10;
            _rectangle.Y2 = 20;
            _rectangle.Draw(_iGraphics);
            Assert.AreEqual(10, _iGraphics.X1);
            Assert.AreEqual(10, _iGraphics.Y1);
            Assert.AreEqual(20, _iGraphics.X2);
            Assert.AreEqual(20, _iGraphics.Y2);
        }
        [TestMethod()]
        public void DrawFrameTest()
        {
            _rectangle.X1 = 10;
            _rectangle.X2 = 20;
            _rectangle.Y1 = 10;
            _rectangle.Y2 = 20;
            _rectangle.DrawFrame(_iGraphics);
            Assert.AreEqual(10, _iGraphics.X1);
            Assert.AreEqual(10, _iGraphics.Y1);
            Assert.AreEqual(20, _iGraphics.X2);
            Assert.AreEqual(20, _iGraphics.Y2);
        }

        [TestMethod()]
        public void IsContainTest()
        {
            _rectangle.X1 = 10;
            _rectangle.X2 = 20;
            _rectangle.Y1 = 10;
            _rectangle.Y2 = 20;
            Assert.IsTrue(_rectangle.IsContain(15, 15));
            _rectangle.X1 = 20;
            _rectangle.X2 = 10;
            _rectangle.Y1 = 20;
            _rectangle.Y2 = 10;
            Assert.IsTrue(_rectangle.IsContain(14, 13));
            _rectangle.X1 = 10;
            _rectangle.X2 = 5;
            _rectangle.Y1 = 10;
            _rectangle.Y2 = 20;
            Assert.IsFalse(_rectangle.IsContain(1, 1));
            _rectangle.X1 = 10;
            _rectangle.X2 = 20;
            _rectangle.Y1 = 10;
            _rectangle.Y2 = 5;
            Assert.IsFalse(_rectangle.IsContain(100, 100));
        }
        [TestMethod()]
        public void ShapeTest()
        {
            IShape rectangle = new Rectangle();
            Assert.AreEqual(new Rectangle().GetType(), Factory.CopyShape(rectangle).GetType());
        }
    }
}