﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DrawingModelTests
{
    [TestClass()]
    public class ShapeBuilderTest
    {
        ShapeBuilder _builder;
        Line _line;
        Hexagon _hexagon;
        Rectangle _rectangle;

        [TestInitialize]
        public void initialize()
        {
            _builder = new ShapeBuilder();

            _builder.BuildLine(10, 20, 30, 40);
            _line = (Line)_builder.GetResult();

            _builder.BuildRectangle(20, 30, 40, 50);
            _rectangle = (Rectangle)_builder.GetResult();

            _builder.BuildHexagon(30, 40, 50, 60);
            _hexagon = (Hexagon)_builder.GetResult();
            
        }

        [TestMethod()]
        public void buildLineTest()
        {
            _builder.BuildLine(10, 20, 30, 40);
            IShape shape = _builder.GetResult();
            Assert.AreEqual(10, shape.X1);
            Assert.AreEqual(20, shape.Y1);
            Assert.AreEqual(30, shape.X2);
            Assert.AreEqual(40, shape.Y2);
            Assert.AreEqual(typeof(Line), shape.GetType());
        }

        [TestMethod()]
        public void buildRectangleTest()
        {
            _builder.BuildRectangle(10, 20, 30, 40);
            IShape shape = _builder.GetResult();
            Assert.AreEqual(10, shape.X1);
            Assert.AreEqual(20, shape.Y1);
            Assert.AreEqual(30, shape.X2);
            Assert.AreEqual(40, shape.Y2);
            Assert.AreEqual(typeof(Rectangle), shape.GetType());
        }

        [TestMethod()]
        public void buildHexagonTest()
        {
            _builder.BuildHexagon(10, 20, 30, 40);
            IShape shape = _builder.GetResult();
            Assert.AreEqual(10, shape.X1);
            Assert.AreEqual(20, shape.Y1);
            Assert.AreEqual(30, shape.X2);
            Assert.AreEqual(40, shape.Y2);
            Assert.AreEqual(typeof(Hexagon), shape.GetType());
        }

        [TestMethod()]
        public void writeJson()
        {
            _builder.BuildLine(10, 20, 30, 40);
            IShape shape = _builder.GetResult();

            JObject jObject = new JObject(); // json object to embed lines

            jObject.Add("Line", JObject.FromObject(shape)); //turn ishape to json object and add it to jObject
            
            string output = JsonConvert.SerializeObject(jObject, Formatting.Indented);

            System.IO.File.WriteAllText("../../json/output.json", output);

            /* output.json 
             * 
             * {
             *   "Line": {
             *      "X1": 10.0,
             *       "Y1": 20.0,
             *       "X2": 30.0,
             *       "Y2": 40.0
             *   }
             * }
             */

            Assert.AreEqual(typeof(Line), shape.GetType()); //get shape type from ishape should be line
        }

        [TestMethod()]
        public void shapes2Json()
        {
            List<IShape> shapes = new List<IShape>();
            
            JArray lines = new JArray();
            JArray hexagons = new JArray();
            JArray rectangles = new JArray();

            shapes.Add(_line);
            shapes.Add(_rectangle);
            shapes.Add(_rectangle);
            shapes.Add(_hexagon);

            foreach(IShape shape in shapes)
            {
                if(shape.GetType() == typeof(Line))
                {
                    lines.Add(JToken.FromObject(shape));   
                }
                else if(shape.GetType() == typeof(Rectangle))
                {
                    rectangles.Add(JToken.FromObject(shape));
                }
                else if(shape.GetType()== typeof(Hexagon))
                {
                    hexagons.Add(JToken.FromObject(shape));
                }

            }

            JObject outputShapes = new JObject();
            
            outputShapes.Add("Line", lines);
            outputShapes.Add("Rectangle", rectangles);
            outputShapes.Add("Hexagon", hexagons);

            string output = JsonConvert.SerializeObject(outputShapes, Formatting.Indented);
            System.IO.File.WriteAllText("../../json/shapes_output.json", output);

        }


    }
}
