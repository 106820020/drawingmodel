﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawingModel;
using System.IO;

namespace DrawingModelTests
{
    [TestClass()]
    public class ShapeParserTests
    {
        private string _inputPath;
        private string _lineData;
        private string _rectangleData;
        private string _hexagonData;


        [TestInitialize]
        public void initialize()
        {
            _inputPath = "../../json/input.json";
            _lineData = "../../json/lineData.json";
            _rectangleData = "../../json/rectangleData.json";
            _hexagonData = "../../json/hexagonData.json";
        }

        [TestMethod()]
        public void line_parsing_by_shapeParser()
        {
            ShapeParser parser = new ShapeParser();
            try
            {
                //parser.parse(_lineData);
                parser.parse(_inputPath);
                List<IShape> i =  parser.getResult();
                Assert.AreEqual(6, i.Count);
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.StackTrace);

            }
        }
    }
}
