﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DrawingModel.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DrawingModel.Commands;

namespace DrawingModel.States.Tests
{
    [TestClass()]
    public class DrawingStateTests
    {
        //ut
        [TestMethod()]
        public void PressPointerTest()
        {
            Model.Instance().mode = Mode.Rectangle;
            Model.Instance().PressPointer(10, 10);
            Model.Instance().MovePointer(20, 20);
        }
    }
}