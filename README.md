# Design pattern — Term project
## drawing model

Team members:

-   106820020 郭育瑋
-   106820039 邱柏政
-   106820037 羅育陞

# Problem Statement
Windows原本內建的小畫家並不能直接點選已畫好的圖形，也因此不能直接對原圖形做修改操作。在視窗程式設計課中，我們實作了小畫家，基於此程式再加上我們想改進之處。我們希望可以保留視窗設計課擁有的單點選取的能力，並且新增合併圖形、刪除等功能，讓使用者可以更自由的編輯與創作。

# Demo

![video](video/demo.mp4)

# Design Patterns in Our Code

![image](img/force.jpg)

## Singleton

- Motivation : 為了確保model和command manager不能重複產生，為了確保他是唯一的，能避免不小心多宣告導致資料不一。且因大部分的function需要model，參數會太多。

- Solution : 使用 *Singleton* 和 *private constructor* 讓model和command manager確實只有一個實體。

- Consequence :
	+ 因為使用了 *private constructor*，所以讓model和command manager確實只有一個實體。
		+ model是儲存view(畫布)的一切資料ex:command(圖形)、所有邏輯，我們的畫布只有一張，因此model也只有一個。
		+ command manager是command的管理人，所有指令都必須經過他，才能完整做出redo/undo，因此command manager也只能有一個。
	+ 減少傳遞的參數。
- Use : Model.cs 和 CommandManager.cs

## Command

- Motivation : 小畫家需要有 undo/redo 的功能。

- Solution : 使用 *Command* 把動作要求封裝成一個物件，使其可以將動作順序儲存。

- Consequence :
	+ 系統複雜度增加
	+ 更優雅的處理一個 行為(彈性大)
	+ 實作 undo/redo 容易
![image](img/Command.png)
- Use : Command資料夾裡所有檔案

## Builder and Parser

- Motivation : 將畫布中的圖形匯出成Json檔儲存起來，以便在下次開啟時能夠匯入Json檔，將上一次所繪製出的圖形直接放回至畫布中。

- Solution : 透過 *Parser* 將儲存至Json檔，依據圖形的類別選擇相對應的 *Builder* 來建置圖形，再將完成的圖形加入到畫布中。透過 *Builder* 能夠隨意組建複雜的圖形。

- Consequence :
	+ 能夠將圖形的構造及表示分開。
	+ 提供建製圖形過程步驟的控制。

- Output Json :

```json
[
  {
    "type": "Compound",
    "X1": 934.0,
    "Y1": 88.0,
    "X2": 1440.0,
    "Y2": 551.0,
    "content": [
      {
        "type": "Rectangle",
        "X1": 934.0,
        "Y1": 88.0,
        "X2": 1440.0,
        "Y2": 266.0
      },
      {
        "type": "Hexagon",
        "X1": 988.0,
        "Y1": 397.0,
        "X2": 1147.0,
        "Y2": 501.0
      },
      {
        "type": "Line",
        "X1": 1197.0,
        "Y1": 320.0,
        "X2": 1363.0,
        "Y2": 509.0
      }
    ]
  },
  {
    "type": "Rectangle",
    "X1": 185.0,
    "Y1": 104.0,
    "X2": 338.0,
    "Y2": 177.0
  },
  {
    "type": "Hexagon",
    "X1": 364.0,
    "Y1": 246.0,
    "X2": 495.0,
    "Y2": 381.0
  },
  {
    "type": "Line",
    "X1": 210.0,
    "Y1": 371.0,
    "X2": 355.0,
    "Y2": 488.0
  }
]
```

## Composite

- Motivation : 加入合併圖形的功能，希望能夠將我們所繪製的圖形組成一種新的形狀，方便操作與繪製。

- Solution : *Composite* 允許物件合成樹狀結構，呈現「部份 / 整體」的階層關係。

- Consequence :
	+ 讓物件合成樹狀結構。
	+ 能以一致的方式處理個別物件，以及合成的物件。

![video](video/composite.mp4)

## Simple Factory

- Motivation : 將所有生產圖形的工作統一

- Solution : 使用 *Simple Factory* 並建立靜態方法以生產各種圖形

- Consequence :
	+ 降低程式碼的耦合度
	+ 分工明確
![image](img/Composite_SimpleFactory.png)

```C#
public static IShape ConstructShape(String shape)
{
    if (shape == "Line")
        return new Line();
    else if (shape == "Rectangle")
        return new Rectangle();
    else if (shape == "Hexagon")
        return new Hexagon();
    else if (shape == "Compound")
        return new Compound();
    else
        return null;
}
```

## State

- Motivation : 因為小畫家有許多狀態，使用大量條件語句，且不好管理。

- Solution : 為了有效管理這些狀態，使用 *State* 來管理各個狀態，目前有6個State。

- Consequence :
	+ 擴充方便
	+ 管理容易
	+ 將每個狀態轉換和動作封裝在一個class中，可以將執行狀態的概念提升為完整的對象狀態。
![image](img/State.jpg)

```C#
// click clear button
public void ClickClearButton(object sender, System.EventArgs e)
{
    Model.Instance().SetState(Model.Instance().GetClearState());
    Model.Instance().Clear();
    _presentationModel.ClickClear();
    UpdateButtons();
    _selectInfo.Text = _presentationModel.GetSelectInfo();
}

// click rectangle button
private void ClickRectangleButton(object sender, EventArgs e)
{
    Model.Instance().SetState(Model.Instance().GetDrawState());
    Model.Instance().mode = Mode.Rectangle;
    _presentationModel.ClickRectangle();
    UpdateButtons();
}

// click line button
private void ClickLineButton(object sender, EventArgs e)
{
    Model.Instance().SetState(Model.Instance().GetDrawState());
    Model.Instance().mode = Mode.Line;
    _presentationModel.ClickRectangle();
    UpdateButtons();
}

// click line button
private void ClickHexagonButton(object sender, EventArgs e)
{
    Model.Instance().SetState(Model.Instance().GetDrawState());
    Model.Instance().mode = Mode.Hexagon;
    _presentationModel.ClickRectangle();
    UpdateButtons();
}

private void ClickMutiSelectButton(object sender, EventArgs e)
{
    Model.Instance().SetState(Model.Instance().GetMutiSelectState());
    _presentationModel.ClickMutiSelect();
    UpdateButtons();
    _selectInfo.Text = _presentationModel.GetSelectInfo();
}

private void ClickCombineButton(object sender, EventArgs e)
{
    Model.Instance().ClickCombine();
    Model.Instance().SetState(Model.Instance().GetCursorState());
    _presentationModel.ClickCombine();
    UpdateButtons();
    _selectInfo.Text = _presentationModel.GetSelectInfo();
}
```
```C#

private Model()
{
    CommandManager.Instance();
    _lineState = new LineState();
    _drawState = new DrawState();
    _mutiSelectState = new MutiSelectState();
    _clearState = new ClearState();
    _state = _cursorState;
}

public void SetState(State state)
{
    _state.Init();
    _state = state;
}

//SetCursorState
public State GetCursorState()
{
    return _cursorState;
}

public State GetDrawState()
{
    return _drawState;
}

//SetMutiSelectState
public State GetMutiSelectState()
{
    return _mutiSelectState;
}

//SetClearState
public State GetClearState()
{
    return _clearState;
}
```
